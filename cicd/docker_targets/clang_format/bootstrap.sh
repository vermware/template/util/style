#!/bin/sh

set -eu

apt-get update

apt-get install --no-install-recommends -y \
	clang-format

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
